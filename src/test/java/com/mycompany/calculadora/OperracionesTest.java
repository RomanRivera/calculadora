/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculadora;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roman
 */
public class OperracionesTest {
    
    public OperracionesTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of suma method, of class Operraciones.
     */
    @Test
    public void testSuma() {
        System.out.println("suma");
        int a = 8;
        int b = 4;
        Operraciones instance = new Operraciones();
        int expResult = 12;
        int result = instance.suma(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      if(result !=expResult){
         fail("The test case is a prototype."); 
   }
    }

    /**
     * Test of resta method, of class Operraciones.
     */
    @Test
    public void testResta() {
        System.out.println("resta");
        int a = 5;
        int b = 10;
        Operraciones instance = new Operraciones();
        int expResult = -5;
        int result = instance.resta(a, b);
        assertEquals(expResult, result);
   if(result !=expResult){
         fail("The test case is a prototype."); 
   }
      
    }

    /**
     * Test of multiplicaion method, of class Operraciones.
     */
    @Test
    public void testMultiplicaion() {
        System.out.println("multiplicaion");
        int a = 7;
        int b = 1;
        Operraciones instance = new Operraciones();
        int expResult = 7;
        int result = instance.multiplicaion(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      if(result !=expResult){
         fail("The test case is a prototype."); 
   }
      
    }

    /**
     * Test of divicion method, of class Operraciones.
     */
    @Test
    public void testDivicion() {
        System.out.println("divicion");
        int a = 16;
        int b = 2;
        Operraciones instance = new Operraciones();
        int expResult = 8;
        int result = instance.divicion(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       if(result !=expResult){
         fail("The test case is a prototype."); 
   }
      
    }
}
